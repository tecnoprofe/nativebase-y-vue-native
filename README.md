# CALCULADORA MATRICIAL

Esta es una calculadora matricial, que realiza, la suma, resta y multiplicación de matrices.

![](./assets/portada_readme.png)
# INTALACIÓN Y USO
## PASO 1. Instalar laragon
***
Para este proyecto, se recomienda tener instalado [Laragon - Full (147 MB)](https://laragon.org/download/) 
## PASO 2. Carpeta de trabajo
***
Tener una carpeta de trabajo, por ejemplo en windows:
```bash
cd C:\tecnoprofe
```
## PASO Clonar proyecto
***
Descargar el proyecto, con el siguiente comando.
```bash
git clone https://gitlab.com/tecnoprofe/nativebase-y-vue-native.git
cd C:\tecnoprofe\nativebase-y-vue-native
```
***
Instalación
```bash
npm install
```
## PASO 4. Ejecución de la aplicación
```bash
npm start
```
***
Es recomendable contar con expo-cli instalado de manera global, para usar el siguiente comando, que produce el mismo resultado. 
```bash
expo start
```
# SIGUEME EN MIS REDES SOCIALES
- [Visita mi canal de youtube ](https://www.youtube.com/tecnoprofe)
- [Sigueme en Facebook](https://www.facebook.com/zambranachaconjaime/)

